import { HomePage } from './../pages/home/home';
import { firebaseConfig } from './credentials';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { AdminHomePage } from '../pages/admin-home/admin-home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp(firebaseConfig);
    const unsubscribe = firebase.auth().onAuthStateChanged(user => { if (!user) { this.rootPage = 'LoginPage'; unsubscribe(); } else { 
       if(user.email=='dotnext24@gmail.com')
       {       
      this.rootPage = AdminHomePage; 
      unsubscribe(); 
       }
      else
      {
      this.rootPage=HomePage;
      unsubscribe(); 
      }
    } });

  }


}


