import { DirectivesModule } from './../directives/directives.module';
import { CategoryPageModule } from './../pages/category/category.module';
import { LoginPage } from './../pages/login/login';
import { TextAvatarDirective } from '../directives/text-avatar/text-avatar';
import { AdminHomePageModule } from '../pages/admin-home/admin-home.module';
import { AdminHomePage } from './../pages/admin-home/admin-home';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';
import { EventProvider } from '../providers/event/event';
import { ProfileProvider } from '../providers/profile/profile';
import {Camera} from '@ionic-native/camera'
import { CategoryProvider } from '../providers/category/category';
import { SubCategoryProvider } from '../providers/sub-category/sub-category';
import { CartProvider } from '../providers/cart/cart';
import { DespacitoProvider } from '../providers/despacito/despacito';
import { TransactionProvider } from '../providers/transaction/transaction';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    AdminHomePageModule, 
    DirectivesModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AdminHomePage
  ],
  providers: [
    Camera,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    EventProvider,
    ProfileProvider,
    CategoryProvider,
    SubCategoryProvider,
    CartProvider,
    DespacitoProvider,
    TransactionProvider
  ]
})
export class AppModule {}
