import { TransactionProvider } from './../../providers/transaction/transaction';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-admin-home',
  templateUrl: 'admin-home.html',
})
export class AdminHomePage {

  public transactionList:Array<any>=[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public transactionProvider:TransactionProvider
  ) {

   
  }


  goToCategory(): void {
    this.navCtrl.push('CategoryListPage');
  }

  goToDespacito(): void {
    this.navCtrl.push('DespacitoListPage');
  }

  logoutUser(): void {
    this.authProvider.logoutUser().then(() => {
      this.navCtrl.setRoot(LoginPage);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminHomePage');
    this.transactionProvider.getDespacitoList().on('value',transactionListSnap=>{
       transactionListSnap.forEach(snap=>{
         this.transactionList.push({
           mobile:snap.val().customerMobile,
           totalAmount:snap.val().totalAmount,
           paymentMode:snap.val().paymentMode,
           date:snap.val().date,
           items:snap.val().itemList
         })
         return false;
       })


    })
  }


}
