import { CartItem, CartProvider } from '../../providers/cart/cart';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  public cartItemList:Array<CartItem>=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
     public cartProvider:CartProvider
  ) {
  }

  checkout(){  
      this.navCtrl.push('CheckoutPage');
}

getTotal(): number{
    return this.cartProvider.getGrandTotal();
}

removeItemFromCart(item){
  this.cartProvider.removeItemById(item.id);    
}

quantityPlus(item){
    this.cartProvider.quantityPlus(item);
}

quantityMinus(item){
  if(item.quantity>1)
  this.cartProvider.quantityMinus(item);
}


goToCheckoutPage()
{

}



  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
    this.cartItemList=[];
    this.cartItemList=this.cartProvider.getAllCartItems();
  }

}
