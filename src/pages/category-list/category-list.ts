import { CategoryProvider } from '../../providers/category/category';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';


@IonicPage({
  segment:'category-list/:despacitoId'
})
@Component({
  selector: 'page-category-list',
  templateUrl: 'category-list.html',
})
export class CategoryListPage {
  public categoryList: Array<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public categoryProvider: CategoryProvider
  ) {
  }


  createCategory(): void {
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        placeholder: 'Category name?'
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            const despacitoId=this.navParams.get('despacitoId');
            this.categoryProvider.createCategory(despacitoId, data.name).then(newCategory=>{
              this.categoryProvider.getCategoryList(despacitoId).once('value').then(categoryListSnapshot => {
                
                      categoryListSnapshot.forEach(snap => {
                        this.categoryList.push({
                          id: snap.key,
                          name: snap.val().name
                        })
                        return false;
                      })
                
                
                    })
            })
          }
        }]
    });

    alert.present();
  }

  updateCategory(categoryId,categoryName): void {
    
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        value:categoryName
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            this.categoryProvider.updateCategory(this.navParams.get('despacitoId'), categoryId,data.name).then(newCategory=>{
              //refresh
              this.categoryList = [];
              this.categoryProvider.getCategoryList(this.navParams.get('despacitoId')).once('value').then(categoryListSnapshot => {
          
                categoryListSnapshot.forEach(snap => {
                  this.categoryList.push({
                    id: snap.key,
                    name: snap.val().name
                  })
                  return false;
                })
              });
             //end refresh
            })
          }
        },
        {
          text:'Delete',
          handler:data=>{
            this.categoryProvider.removeCategory(this.navParams.get('despacitoId'),categoryId).then(newCategory=>{
              //refresh
              this.categoryList = [];
              this.categoryProvider.getCategoryList(this.navParams.get('despacitoId')).once('value').then(categoryListSnapshot => {
          
                categoryListSnapshot.forEach(snap => {
                  this.categoryList.push({
                    id: snap.key,
                    name: snap.val().name
                  })
                  return false;
                })
              });
             //end refresh
            })
          }
        }]
    });

    alert.present();
  }


  goToSubCategoryPage(categoryId:string)
  {
     this.navCtrl.push('SubCategoryListPage',{despacitoId:this.navParams.get('despacitoId'), categoryId:categoryId})
  }


  ionViewDidLoad() {
    this.categoryList = [];
    this.categoryProvider.getCategoryList(this.navParams.get('despacitoId')).once('value').then(categoryListSnapshot => {

      categoryListSnapshot.forEach(snap => {
        this.categoryList.push({
          id: snap.key,
          name: snap.val().name
        })
        return false;
      })


    })

    console.log('ionViewDidLoad CategoryListPage');
  }

}
