import { DirectivesModule } from '../../directives/directives.module';
import { TextAvatarDirective } from '../../directives/text-avatar/text-avatar';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryPage } from './category';

@NgModule({
  declarations: [
    CategoryPage
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(CategoryPage),
  ],
})
export class CategoryPageModule {}
