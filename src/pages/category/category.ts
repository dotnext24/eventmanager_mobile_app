import { DespacitoProvider } from '../../providers/despacito/despacito';
import { CategoryProvider } from '../../providers/category/category';
import { ProfileProvider } from '../../providers/profile/profile';
import { AuthProvider } from '../../providers/auth/auth';
import { CartItem, CartProvider } from '../../providers/cart/cart';
import { Component } from '@angular/core';
import { IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';



@IonicPage({
  segment:'category/:despacitoId'
})
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  public categoryList:Array<any>=[];
  public cartItemList:Array<CartItem>=[];
  public currentDespacitoId:string;
  public currentDespacitoName:string;
  public loading:Loading;
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public authProvider:AuthProvider,
    public profileProvideter:ProfileProvider,   
    public cartProvider:CartProvider,
    public categoryProvider:CategoryProvider,
    public despacitoProvider:DespacitoProvider,
    public loadingCtrl:LoadingController
   ) {
    this.loading=this.loadingCtrl.create();
    this.loading.present();
  }

  goToMenuPage(despacitoId:string,categoryId:string):void
  {
     this.navCtrl.push('MenuPage',{despacitoId:despacitoId,categoryId:categoryId});
  }

  goToCartPage():void
  {
    this.navCtrl.push('CartPage')
  }
  getCartTotalQuantity(cartItems):number
  {
    return  cartItems.map(item => item.quantity).reduce((prev, next) => prev + next);
  }
  ionViewDidLoad() {

    console.log('ionViewDidLoad CategoryPage');

    this.currentDespacitoId=this.navParams.get('despacitoId');

    //current despacito
    this.despacitoProvider.getDespacitoDetail(this.currentDespacitoId).on('value',despacito=>{
            this.currentDespacitoName=despacito.val().name;
    })


    this.categoryList = [];
    this.categoryProvider.getCategoryList(this.navParams.get('despacitoId')).once('value').then(categoryListSnapshot => {

      categoryListSnapshot.forEach(snap => {
        this.categoryList.push({
          id: snap.key,
          name: snap.val().name
        })
        return false;
      })


    })

    //cart list
    this.cartItemList=this.cartProvider.getAllCartItems();

    this.loading.dismiss();
  }

}
