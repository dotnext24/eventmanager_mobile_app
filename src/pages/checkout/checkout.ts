import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { tryCatch } from 'rxjs/util/tryCatch';
import { TransactionProvider } from './../../providers/transaction/transaction';
import { CartProvider } from '../../providers/cart/cart';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
 

  public checkouitForm:FormGroup;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public cartProvider:CartProvider,
    public transactionProvider:TransactionProvider,
    public alertCtrl:AlertController,
    public formBuilder:FormBuilder
  
  ) {

    this.checkouitForm=this.formBuilder.group({
      mobile:['',Validators.compose([Validators.required])],
      paymentMode:['cash',Validators.compose([Validators.required])]
    })
  }

  checkout():void
  {
     //check validation
     if(!this.checkouitForm.valid)
     {
         console.log('invalid form');
     }  
     else
     {

      
       const cartItems=this.cartProvider.getAllCartItems();
       const mobile=this.checkouitForm.value.mobile;
       const paymentMode=this.checkouitForm.value.paymentMode;

       this.transactionProvider.createTransaction(mobile, this.cartProvider.getGrandTotal(),paymentMode,'',cartItems).then(transaction=>{
           const alert:Alert=this.alertCtrl.create({
             message:'Submitted successfully!',
             buttons:[{
               text:'Ok',
               handler:()=>{
                 this.cartProvider.emptyCart();
                 this.navCtrl.setRoot(HomePage);
               }
               
             }]
             
           })

           alert.present();
       },error=>{   
        
          const alert:Alert=this.alertCtrl.create({
            message:error.message,
            buttons:[{
              text:'Ok'
            }]            
          })  
          alert.present();        
       })
      
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

}
