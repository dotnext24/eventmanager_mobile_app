import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DespacitoListPage } from './despacito-list';

@NgModule({
  declarations: [
    DespacitoListPage,
  ],
  imports: [
    IonicPageModule.forChild(DespacitoListPage),
  ],
})
export class DespacitoListPageModule {}
