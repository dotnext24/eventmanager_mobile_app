import { DespacitoProvider } from './../../providers/despacito/despacito';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-despacito-list',
  templateUrl: 'despacito-list.html',
})
export class DespacitoListPage {

  public despacitoList:Array<any>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public despacitoProvider:DespacitoProvider,
    public alertCtrl:AlertController
  ) {
  }


  createDespacito(): void {
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        placeholder: 'Despacito name?'
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            this.despacitoProvider.createDespacito(data.name).then(newDespacito=>{
              this.despacitoProvider.getDespacitoList().once('value').then(despacitoListSnapshot => {
                this.despacitoList = []; 
                despacitoListSnapshot.forEach(snap => {
                        this.despacitoList.push({
                          id: snap.key,
                          name: snap.val().name
                        })
                        return false;
                      })
                
                
                    })
            })
          }
        }]
    });

    alert.present();
  }

  updateDespacito(despacitoId,despacitoName): void {
    
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        value:despacitoName
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            this.despacitoProvider.updateDespacito(despacitoId,data.name).then(newDespacito=>{
              //refresh
              this.despacitoList = [];
              this.despacitoProvider.getDespacitoList().once('value').then(despacitoListSnapshot => {
          
                despacitoListSnapshot.forEach(snap => {
                  this.despacitoList.push({
                    id: snap.key,
                    name: snap.val().name
                  })
                  return false;
                })
              });
             //end refresh
            })
          }
        },
        {
          text:'Delete',
          handler:data=>{
            this.despacitoProvider.removeDespacito(despacitoId).then(newDespacito=>{
              //refresh
              this.despacitoList = [];
              this.despacitoProvider.getDespacitoList().once('value').then(despacitoListSnapshot => {
          
                despacitoListSnapshot.forEach(snap => {
                  this.despacitoList.push({
                    id: snap.key,
                    name: snap.val().name
                  })
                  return false;
                })
              });
             //end refresh
            })
          }
        }]
    });

    alert.present();
  }

  goToCategoryPage(despacitoId:string)
  {
     this.navCtrl.push('CategoryListPage',{despacitoId:despacitoId})
  }
  ionViewDidLoad() {
    this.despacitoList = [];
    this.despacitoProvider.getDespacitoList().once('value').then(despacitoListSnapshot => {

      despacitoListSnapshot.forEach(snap => {
        this.despacitoList.push({
          id: snap.key,
          name: snap.val().name
        })
        return false;
      })


    })
    console.log('ionViewDidLoad DespacitoListPage');
  }

}
