import { EventProvider } from './../../providers/event/event';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html',
})
export class EventListPage {
   public eventList:Array<any>;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
  public eventProvider:EventProvider) {
  }

  goToEventDdatilPage(eventId:string):void
  {
    this.navCtrl.push('EventDetailPage',{eventId:eventId});
  }


  ionViewDidLoad() {
     
    this.eventProvider.getEventList().on('value',eventListSnapshot=>{
      this.eventList=[];
      eventListSnapshot.forEach(snap=>{
        this.eventList.push({
          id:snap.key,
          name:snap.val().name,
          price:snap.val().price,
          date:snap.val().date
        })
        return false;
      })
    })


    console.log('ionViewDidLoad EventListPage');
  }

}
