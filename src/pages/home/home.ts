import { Loading } from 'ionic-angular';
import { DespacitoProvider } from './../../providers/despacito/despacito';
import { CartItem, CartProvider } from '../../providers/cart/cart';
import { CategoryProvider } from '../../providers/category/category';
import { ProfileProvider } from '../../providers/profile/profile';
import { LoginPage } from './../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   public despacitoList:Array<any>;
   public cartItemList:Array<CartItem>=[];
   public loading:Loading;
  constructor(public navCtrl: NavController,
    public authProvider:AuthProvider,
    public profileProvideter:ProfileProvider,   
    public cartProvider:CartProvider,
    public despacitoProvider:DespacitoProvider,
    public loadingCtrl:LoadingController
  ) {
      this.loading=this.loadingCtrl.create();
      this.loading.present();
  }

  goToProfile():void
  {
    this.navCtrl.push('ProfilePage')
  }

  goToCreateEventPage():void
  {
      this.navCtrl.push('EventCreatePage');
  }

  goToEventListPage():void
  {
    this.navCtrl.push('EventListPage');
  }

  goToCategoryPage(despacitoId:string):void
  {
    this.navCtrl.push('CategoryPage',{despacitoId:despacitoId});
  } 
  goToCartPage():void
  {
    this.navCtrl.push('CartPage')
  }

   logoutUser():void
   {
       this.authProvider.logoutUser().then(()=>{
         this.navCtrl.setRoot(LoginPage);
       })
   }
   getCartTotalQuantity(cartItems):number
   {
     return  cartItems.map(item => item.quantity).reduce((prev, next) => prev + next);
   }
   ionViewDidLoad(){    
    this.despacitoList = [];

    this.despacitoProvider.getDespacitoList().once('value').then(categoryListSnapshot => {

     

      categoryListSnapshot.forEach(snap => {
        this.despacitoList.push({
          id: snap.key,
          name: snap.val().name
        })
        return false;
      })

      this.loading.dismiss();

    })

    //cart list
    this.cartItemList=this.cartProvider.getAllCartItems();
   }
}
