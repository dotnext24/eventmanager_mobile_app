import { EmailValidator } from './../../validators/email';

import { HomePage } from '../home/home';
import { AuthProvider } from './../../providers/auth/auth';

import { Component, Directive } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert,AlertController,Loading,LoadingController } from 'ionic-angular';
import {FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { AdminHomePage } from '../admin-home/admin-home';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public loginForm: FormGroup; 
  public loading:Loading;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl:LoadingController,
    public alertCtrl:AlertController,
    public authProvider:AuthProvider,
    public formBuilder:FormBuilder
    ) 
    {
      //set formGroup
       this.loginForm=this.formBuilder.group({
        email:['',Validators.compose([Validators.required,EmailValidator.IsValid])],
        password:['',Validators.compose([Validators.required,Validators.minLength(6)])]
       })

    }

    
    goToSignUp():void
    {
      this.navCtrl.push('SignupPage');
    }
    goToPasswordReset():void
    {
      this.navCtrl.push('ResetPasswordPage');
    }

    loginUser():void
    {
      //check validation
      if(!this.loginForm.valid)
      {
          console.log('invalid form');
      }
      else{
        //get emai;l and password from formGroup
        const email=this.loginForm.value.email;
        const password=this.loginForm.value.password;

        this.authProvider.loginUser(email,password).then(authData=>{
           this.loading.dismiss().then(()=>{
             if(email=='dotnext24@gmail.com')
             this.navCtrl.setRoot(AdminHomePage);
             else
             this.navCtrl.setRoot(HomePage);
           })
        },
        errer=>{
           this.loading.dismiss().then(()=>{
              const alert:Alert=this.alertCtrl.create({
                message:errer.message,
                buttons:[{text:'OK',role:'cancel'}]
              });

              alert.present();
           });
        }
      );
      
      this.loading=this.loadingCtrl.create();
      this.loading.present(); 

      }
    }



  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
