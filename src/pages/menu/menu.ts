import { DespacitoProvider } from '../../providers/despacito/despacito';
import { CartPage } from '../cart/cart';
import { CartItem, CartProvider } from './../../providers/cart/cart';
import { SubCategoryProvider } from '../../providers/sub-category/sub-category';
import { CategoryProvider } from '../../providers/category/category';
import { Component } from '@angular/core';
import { AlertController, IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';


@IonicPage({
  segment:'menu/:despacitoId/:categoryId'
})
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  public subCategoryList: Array<any>;
  public currentCategory:any={}; 
  public cartItemList:Array<CartItem>=[];
  public currentDespacito:any={};
  public loading:Loading;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public categoryProvider:CategoryProvider,
    public subCategoryProvider: SubCategoryProvider,
    public cartProvider:CartProvider,
    public despacitoProvider:DespacitoProvider    
    ) {

      this.loading=this.loadingCtrl.create();
      this.loading.present();
  }

  
  addToCart(item):void{
    console.log(item);        
    this.cartProvider.addItem(item,this.currentCategory.name,this.currentDespacito.name, 1);   
  
}

goToCartPage():void
{
  this.navCtrl.push('CartPage')
}

getCartTotalQuantity(cartItems):number
{
  return  cartItems.map(item => item.quantity).reduce((prev, next) => prev + next);
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubCategoryListPage');
    this.subCategoryList = [];
    const currentCategoryId=this.navParams.get('categoryId');
    const currentDespacitoId=this.navParams.get('despacitoId');
    
    console.log('currentDespacitoId',currentDespacitoId);

    this.despacitoProvider.getDespacitoDetail(currentDespacitoId).on('value',desp=>{
       this.currentDespacito=desp.val();
       console.log(this.currentDespacito);
    })

    //current category
    this.categoryProvider.getCategoryDetail(currentDespacitoId,currentCategoryId).on('value',snap=>{
      this.currentCategory=snap.val();
    })

  
   //subcategory list
    this.subCategoryProvider.getSubCategoryList(currentDespacitoId,currentCategoryId).once('value').then(subCategoryListSnapshot => {
     
      subCategoryListSnapshot.forEach(snap => {
        this.subCategoryList.push({
          id: snap.key,
          name: snap.val().name,
          price:snap.val().price,
          unit:snap.val().unit
        })
        return false;
      })

   })

  //cart list
this.cartItemList=this.cartProvider.getAllCartItems(); 
this.loading.dismiss();

}

}
