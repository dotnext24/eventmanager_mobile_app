import { AuthProvider } from '../../providers/auth/auth';
import { ProfileProvider } from './../../providers/profile/profile';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public userProfile: any;
  public birthDate: string;
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public profileProvider: ProfileProvider,
    public authProvider: AuthProvider
  ) {
  }

  updateNmae(): void {
    const alert: Alert = this.alertCtrl.create({
      message: 'Your first name and last name',
      inputs: [
        {
          name: 'firstName',
          placeholder: 'Your first name'
        },
        {
          name:'lastName',
          placeholder:'Your last name'
        }],
        buttons:[
          {
            text:'Cancel'
          },
          {
            text:'Save',
            handler:data=>{
              this.profileProvider.updateName(data.firstName,data.lastName)
            }
          }
        ]
    });
    alert.present();
  }

  updateDOB(birthDate:string):void
  {
    this.profileProvider.updateDOB(birthDate).then(()=>{})
  }
  
  ionViewDidLoad() {
    this.profileProvider.gerUserProfile().on('value', userProfileSanapShot => {
      this.userProfile = userProfileSanapShot.val();
      this.birthDate = userProfileSanapShot.val().birthDate;
    })

    console.log('ionViewDidLoad ProfilePage');
  }

}
