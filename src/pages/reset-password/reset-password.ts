import { EmailValidator } from './../../validators/email';
import { AuthProvider } from '../../providers/auth/auth';

import { Component } from '@angular/core';
import { Alert, AlertController , IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
   public resetPasswordForm:FormGroup;
   public loading:Loading;
  constructor(public navCtrl: NavController, 
   public alertCtrl:AlertController,
   public loadingCtrl:LoadingController,
   public authProvider:AuthProvider,
   public formBiulder:FormBuilder
  ) {

    this.resetPasswordForm=this.formBiulder.group({
      email:["",Validators.compose([Validators.required,EmailValidator.IsValid])]
    })
  }

   
  resetPassword():void
  {

    if(!this.resetPasswordForm.valid){
      console.log('Invalid form');
      return;
    }

    const email=this.resetPasswordForm.value.email;
    console.log(email);
    this.authProvider.resetPassword(email).then(user=>{
      const alert:Alert=this.alertCtrl.create({
        message:'Please check your email for password reset link.',
        buttons:[
          {
            text:'OK',
            role:'cancel',
            handler:()=>{
              this.navCtrl.pop();
            }

          }
        ]
      })
      alert.present();
    }).catch(error=>{
      const alert:Alert=this.alertCtrl.create({
        message:error.message,
        buttons:[
          {
            text:'OK',
            role:'cancel'
          }
        ]
      });
      alert.present();
    })
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

}
