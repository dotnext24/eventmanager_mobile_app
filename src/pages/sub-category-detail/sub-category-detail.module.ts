import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubCategoryDetailPage } from './sub-category-detail';

@NgModule({
  declarations: [
    SubCategoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SubCategoryDetailPage),
  ],
})
export class SubCategoryDetailPageModule {}
