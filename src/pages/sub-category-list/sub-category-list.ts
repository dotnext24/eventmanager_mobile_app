import { SubCategoryProvider } from '../../providers/sub-category/sub-category';
import { CategoryProvider } from './../../providers/category/category';
import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';


@IonicPage({
  segment:'sub-category-list/:despacitoId/:categoryId'
})
@Component({
  selector: 'page-sub-category-list',
  templateUrl: 'sub-category-list.html',
})
export class SubCategoryListPage {
  public subCategoryList: Array<any>;
  public currentCategory:any={}; 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public categoryProvider:CategoryProvider,
    public subCategoryProvider: SubCategoryProvider
    ) {
  }

  createSubCategory(): void {
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        placeholder: 'Category name?'
      },
      {
        name: 'price',
        type: 'number',
        placeholder: 'Enter price'
      },
      {
        name: 'unit',
        type: 'text',
        placeholder: 'Enter uniy ex. full/half/2pcs'
      }
    ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            const currentCategoryId=this.navParams.get('categoryId');
            this.subCategoryProvider.createSubCategory(this.navParams.get('despacitoId'),currentCategoryId,data.name,data.price,data.unit).then(newSubCategory=>{
              this.subCategoryProvider.getSubCategoryList(this.navParams.get('despacitoId'),currentCategoryId).once('value').then(subCategoryListSnapshot => {
                this.subCategoryList=[];
                subCategoryListSnapshot.forEach(snap => {
                  this.subCategoryList.push({
                    id: snap.key,
                    name: snap.val().name,
                    price:snap.val().price,
                    unit:snap.val().unit
                  })
                  return false;
                })
                
                
                    })
            })
          }
        }]
    });

    alert.present();
  }

  updateSubCategory(subCategoryId,subCategoryName,price,unit): void {
    
    const alert = this.alertCtrl.create({
      inputs: [{
        name: 'name',
        type: 'text',
        value:subCategoryName
      },
      {
        name: 'price',
        type: 'number',
        value: price
      },
      {
        name: 'unit',
        type: 'text',
        value: unit
      }
    ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text:'Save',
          handler:data=>{
            const currentCategoryId=this.navParams.get('categoryId');
            this.subCategoryProvider.updateSubCategory(this.navParams.get('despacitoId'),currentCategoryId,subCategoryId, data.name, data.price,data.unit).then(newCategory=>{
              //refresh
              this.subCategoryList=[];
              this.subCategoryProvider.getSubCategoryList(this.navParams.get('despacitoId'),currentCategoryId).once('value').then(subCategoryListSnapshot => {
                                      
                      subCategoryListSnapshot.forEach(snap => {
                        this.subCategoryList.push({
                          id: snap.key,
                          name: snap.val().name,
                          price:snap.val().price,
                          unit:snap.val().unit
                        })
                        return false;
                      })
                
                  })
             //end refresh
            })
          }
        },
        {
          text:'Delete',
          handler:data=>{
            const currentCategoryId=this.navParams.get('categoryId');
            this.subCategoryProvider.removeSubCategory(this.navParams.get('despacitoId'),currentCategoryId,subCategoryId).then(newCategory=>{
              //refresh
              this.subCategoryList=[];
              this.subCategoryProvider.getSubCategoryList(this.navParams.get('despacitoId'),currentCategoryId).once('value').then(subCategoryListSnapshot => {
                                    
                      subCategoryListSnapshot.forEach(snap => {
                        this.subCategoryList.push({
                          id: snap.key,
                          name: snap.val().name,
                          price:snap.val().price,
                          unit:snap.val().unit
                        })
                        return false;
                      })
                
                  })
             //end refresh
            })
          }
        }]
    });

    alert.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SubCategoryListPage');
    this.subCategoryList = [];
    const currentCategoryId=this.navParams.get('categoryId');
    console.log('currentCategoryId',currentCategoryId);

    this.categoryProvider.getCategoryDetail(this.navParams.get('despacitoId'),currentCategoryId).on('value',snap=>{
      this.currentCategory=snap.val();
    })

  

    this.subCategoryProvider.getSubCategoryList(this.navParams.get('despacitoId'),currentCategoryId).once('value').then(subCategoryListSnapshot => {
     
      subCategoryListSnapshot.forEach(snap => {
        this.subCategoryList.push({
          id: snap.key,
          name: snap.val().name,
          price:snap.val().price,
          unit:snap.val().unit
        })
        return false;
      })

  })

}
}
