import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class CartProvider {
  list : Array<CartItem>; 
  
  constructor(){
      this.list = []
  }
  
  getAllCartItems(){
      return this.list;
  }
  
  getItemById(id: string){
      for(var i = 0; i < this.list.length; i++){
          if(this.list[i].id == id){
              return this.list[i];
          }
      }
  }


  addItem(item:any,category:string,despacito:string, quantity:number){
    
    var isExists : boolean = false;
    var id = item.id;
    
    for(var i = 0; i < this.list.length; i++){
        if(this.list[i].id == id){
            this.list[i].quantity += quantity;
            isExists = true;
            break;
        }
    }
    if(!isExists){
        this.list.push(new CartItem(item, quantity,category,despacito));
    }
   
}

removeItemById(id){        
    for(var i = 0; i < this.list.length; i++){
        if(this.list[i].id == id){
            this.list.splice(i, 1);
            break;
        }
    }        
}

emptyCart(){        
    for(var i = 0; i < this.list.length; i++){
        this.list.splice(i, 1);
        i--;
    }    
}

quantityPlus(item){
    item.quantity += 1;
}

quantityMinus(item){
    item.quantity -= 1;
}

getGrandTotal(): number{
    var amount = 0;
    for(var i = 0; i < this.list.length; i++){
        amount += (this.list[i].price * this.list[i].quantity);
    }
    return amount;
}

}



export class CartItem {
  id:string;
  name:string;
  price:number;  
  quantity:number;
  category:string;
  despacito:string;

  constructor(item, quantity:number,category:string='n/a',despacito:string='n/a') {
      this.id = item.id;
      this.name = item.name;
      this.price = item.price; 
      this.quantity = quantity;
      this.category=category;
      this.despacito=despacito;
  }
}
