import { stagger } from '@angular/core/src/animation/dsl';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase'

@Injectable()
export class CategoryProvider {
  public categoryRef: firebase.database.Reference;
  constructor() {
    console.log('Hello CategoryProvider Provider');
    this.categoryRef = firebase.database().ref('/despacito');
  }

  createCategory(despacitoId:string, categoryName: string): firebase.database.ThenableReference {
    return this.categoryRef.child(`${despacitoId}/categotyList`).push({
      name: categoryName
    })
  }
 
  updateCategory(despacitoId:string, categoryId:string,categoryName:string):Promise<any>
  {
    return this.categoryRef.child(`${despacitoId}/categotyList/${categoryId}`).update({name:categoryName});
  }


  removeCategory(despacitoId:string, categoryId:string):Promise<any>
  {
    return this.categoryRef.child(`${despacitoId}/categotyList/${categoryId}`).remove();
  }

  getCategoryList(despacitoId:string): firebase.database.Reference {
    return this.categoryRef.child(`${despacitoId}/categotyList`);
  }

  getCategoryDetail(despacitoId:string, categoryId:string): firebase.database.Reference {
    return this.categoryRef.child(`${despacitoId}/categotyList/${categoryId}`);

  }

}
