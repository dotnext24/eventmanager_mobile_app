import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase'


@Injectable()
export class DespacitoProvider {
  public despacitoRef: firebase.database.Reference;
  constructor() {
    console.log('Hello DespacitoProvider Provider');
    this.despacitoRef = firebase.database().ref('/despacito');
  }

  createDespacito(despacitoName: string): firebase.database.ThenableReference {
    return this.despacitoRef.push({
      name: despacitoName
    })
  }


  updateDespacito(despacitoId:string,despacitoName:string):Promise<any>
  {
    return firebase.database().ref(`/despacito/${despacitoId}`).update({name:despacitoName});
  }


  removeDespacito(despacitoId:string):Promise<any>
  {
    return firebase.database().ref(`/despacito/${despacitoId}`).remove();
  }

  getDespacitoList(): firebase.database.Reference {
    return this.despacitoRef;
  }

  getDespacitoDetail(despacitoId): firebase.database.Reference {
    return this.despacitoRef.child(despacitoId);

  }


}
