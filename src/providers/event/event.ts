import { Injectable } from '@angular/core';
import firebase from 'firebase'

@Injectable()
export class EventProvider {
  public eventListRef: firebase.database.Reference;
  constructor() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.eventListRef = firebase.database().ref(`userProfile/${user.uid}/eventList`);
      }
    })
  }

  createEvent(eventName: string, eventDate: string, eventPrice: number, eventCost: number): firebase.database.ThenableReference {
    return this.eventListRef.push({
      name: eventName,
      date: eventDate,
      price: eventPrice,
      cost: eventCost
    });
  }

  getEventList(): firebase.database.Reference {
    return this.eventListRef;
  }

  getEventDetail(eventId: string): firebase.database.Reference {
    return this.eventListRef.child(eventId);
  }

}
