import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase'

@Injectable()
export class SubCategoryProvider {
  public despacitoRef: firebase.database.Reference;
  constructor() {
    console.log('Hello SubCategoryProvider Provider');
    this.despacitoRef = firebase.database().ref('/despacito');
  }

  createSubCategory(despacitoId:string,categoryId:string, subCategoryName: string, price:number,unit:string): firebase.database.ThenableReference {
    
    const subCategoryRef = this.despacitoRef.child(`${despacitoId}/categotyList/${categoryId}/subCategoryList`);
    
    return subCategoryRef.push({
      name: subCategoryName,
      price:price,
      unit:unit
    })
  }
 
  updateSubCategory(despacitoId:string,categoryId:string,subCategoryId:string,subCategoryName: string, price:number,unit:string):Promise<any>
  {
    const subCategoryRef = this.despacitoRef.child(`${despacitoId}/categotyList/${categoryId}/subCategoryList`);
    
    return subCategoryRef.child(subCategoryId).update(
      {name: subCategoryName,
      price:price,
      unit:unit
    });
  }


  removeSubCategory(despacitoId:string,categoryId:string,subCategoryId:string):Promise<any>
  {
    const subCategoryRef = this.despacitoRef.child(`${despacitoId}/categotyList/${categoryId}/subCategoryList`);
    
    return subCategoryRef.child(subCategoryId).remove();
  }

  getSubCategoryList(despacitoId:string, categoryId:string): firebase.database.Reference {
    const subCategoryRef = this.despacitoRef.child(`${despacitoId}/categotyList/${categoryId}/subCategoryList`);
    
    return subCategoryRef;
  }

  getSubCategoryDetail(despacitoId:string, categoryId,subCategoryId:string): firebase.database.Reference {
    const subCategoryRef = this.despacitoRef.child(`${despacitoId}/categotyList/${categoryId}/subCategoryList`);
    
    return subCategoryRef.child(subCategoryId);

  }
}
