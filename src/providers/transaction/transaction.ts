import { CartItem } from './../cart/cart';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase'

@Injectable()
export class TransactionProvider {
  public transactionRef: firebase.database.Reference;
  constructor() {
    console.log('Hello TransactionProvider Provider');
    this.transactionRef = firebase.database().ref('/transaction');
  }


  createTransaction(customerMobile:string,totalAmount:number,paymentMode:string,note: string,cartItemList:Array<CartItem>): firebase.database.ThenableReference {
    return this.transactionRef.push({
      customerMobile: customerMobile,
      totalAmount:totalAmount,
      paymentMode:paymentMode,
      note:note||'',
      date:new Date().toDateString(),
      itemList:cartItemList

    })
  }
  
  getDespacitoList(): firebase.database.Reference {
    return this.transactionRef;
  } 
}
